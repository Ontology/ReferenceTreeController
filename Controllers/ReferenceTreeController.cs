﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.WebSocketServices;
using ReferenceTreeController.BusinessFactory;
using ReferenceTreeController.Models;
using ReferenceTreeController.Services;
using ReferenceTreeController.Translations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;
using System.ComponentModel;

namespace ReferenceTreeController.Controllers
{
    public enum TreeType
    {
        Class = 0,
        Reference = 1
    }
    public class ReferenceTreeController : ReferenceTreeViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private ServiceAgent_Elastic serviceAgentElastic;
        private ViewTreeFactory viewTreeFactory;

        private TranslationController translationController = new TranslationController();

        private TreeType treeType;

        private clsLocalConfig localConfig;

        public OntoMsg_Module.StateMachines.IControllerStateMachine StateMachine { get; private set; }
        public OntoMsg_Module.StateMachines.ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (OntoMsg_Module.StateMachines.ControllerStateMachine)StateMachine;
            }
        }

        public ReferenceTreeController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();

            PropertyChanged += ReferenceTreeViewController_PropertyChanged;
        }

        private void ReferenceTreeViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            if (e.PropertyName == Notifications.NotifyChanges.View_Url_TreeData)
            {
                webSocketServiceAgent.SendPropertyChange(e.PropertyName);
            }
            else if (e.PropertyName == Notifications.NotifyChanges.Navigation_Text_SelectedNodeId)
            {
                webSocketServiceAgent.SendPropertyChange(e.PropertyName);
                var selectNodeRequest = new InterServiceMessage
                {
                    ChannelId = Channels.SelectedClassNode,
                    SenderId = webSocketServiceAgent.EndpointId,
                    OItems = new List<clsOntologyItem>
                    {
                        new clsOntologyItem
                        {
                            GUID = Text_SelectedNodeId
                        }
                    }
                };

                webSocketServiceAgent.SendInterModMessage(selectNodeRequest);
            }
            else if (e.PropertyName == Notifications.NotifyChanges.ViewModel_ViewTreeNodes_PathNodes)
            {
                webSocketServiceAgent.SendPropertyChange(e.PropertyName);
            }
            else if (e.PropertyName == Notifications.NotifyChanges.ViewModel_Text_Search)
            {
                localConfig.SearchNode.Name = "Search: " + Text_Search;
                var classList = serviceAgentElastic.ClassList.Where(clsItem => clsItem.Name.ToLower().Contains(Text_Search.ToLower())).ToList();
                SendTreeLevel(classList, localConfig.SearchNode.Id);
                ViewTreeNodes_PathNodes = new List<Models.ViewTreeNode> { localConfig.SearchNode };
            }
        }

        private void Initialize()
        {
            var stateMachine = new OntoMsg_Module.StateMachines.ControllerStateMachine(OntoMsg_Module.StateMachines.StateMachineType.BlockSelectingSelected | OntoMsg_Module.StateMachines.StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            viewTreeFactory = new ViewTreeFactory(localConfig);
            serviceAgentElastic = new ServiceAgent_Elastic(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;
            localConfig.SearchNode = viewTreeFactory.CrateSearchNode();
        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();

            serviceAgentElastic = null;

            viewTreeFactory = null;
            translationController = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {
            
            var viewType = webSocketServiceAgent.Context.QueryString["ViewType"];
            if (!string.IsNullOrEmpty(viewType.ToString()))
            {
                if (viewType.ToLower() == TreeType.Reference.ToString().ToLower())
                {
                    treeType = TreeType.Reference;
                }
            }

            if (treeType == TreeType.Class)
            {


                webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
                {
                    ChannelTypeId = Channels.AddClassNode,
                    EndPointId = webSocketServiceAgent.EndpointId,
                    EndpointType = EndpointType.Receiver,
                    SessionId = webSocketServiceAgent.DataText_SessionId
                });

                webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
                {
                    ChannelTypeId = Channels.ChangeClassNode,
                    EndPointId = webSocketServiceAgent.EndpointId,
                    EndpointType = EndpointType.Receiver,
                    SessionId = webSocketServiceAgent.DataText_SessionId
                });

                webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
                {
                    ChannelTypeId = Channels.MarkClassNodes,
                    EndPointId = webSocketServiceAgent.EndpointId,
                    EndpointType = EndpointType.Receiver,
                    SessionId = webSocketServiceAgent.DataText_SessionId
                });

                webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
                {
                    ChannelTypeId = Channels.RemoveClassNode,
                    EndPointId = webSocketServiceAgent.EndpointId,
                    EndpointType = EndpointType.Receiver,
                    SessionId = webSocketServiceAgent.DataText_SessionId
                });

                webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
                {
                    ChannelTypeId = Channels.SelectClassNode,
                    EndPointId = webSocketServiceAgent.EndpointId,
                    EndpointType = EndpointType.Receiver,
                    SessionId = webSocketServiceAgent.DataText_SessionId
                });

                var classList = serviceAgentElastic.GetDataClasses();
                SendTreeLevel(serviceAgentElastic.ClassList);
                ViewTreeNodes_PathNodes = new List<Models.ViewTreeNode> { viewTreeFactory.CreateRootNode() };
            }
            else if (treeType == TreeType.Reference)
            {

            }

           
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.Elastic_ResultData)
            {


                if (serviceAgentElastic.ResultData.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    return;


                }

                viewTreeFactory = new ViewTreeFactory(localConfig);
                var treeFileName = Guid.NewGuid().ToString() + ".json";

                var sessionFile = webSocketServiceAgent.RequestWriteStream(treeFileName);

                if (treeType == TreeType.Class)
                {
                    var result = viewTreeFactory.CreateTreeNodes(serviceAgentElastic.ClassList, -1, null);
                    if (result.GUID == localConfig.Globals.LState_Error.GUID)
                    {
                        return;
                    }

                    
                    if (sessionFile.FileUri == null)
                    {
                        return;
                    }

                    if (sessionFile.StreamWriter == null)
                    {

                        return;
                    }

                    using (sessionFile.StreamWriter)
                    {
                        result = viewTreeFactory.WriteJsonDoc(sessionFile.StreamWriter);

                    }
                    if (result.GUID == localConfig.Globals.LState_Error.GUID)
                    {

                        return;
                    }
                }
                


                Url_TreeData = sessionFile.FileUri.AbsoluteUri;
            }
        }

       

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {
                if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.Navigation_Text_SelectedNodeId)
                {
                    if (Text_SelectedNodeId == webSocketServiceAgent.ChangedProperty.Value.ToString()) return;
                    if (!localConfig.Globals.is_GUID(webSocketServiceAgent.ChangedProperty.Value.ToString())) return;
                    Text_SelectedNodeId = webSocketServiceAgent.ChangedProperty.Value.ToString();



                    if (serviceAgentElastic.ClassList.Any(clsItem => clsItem.GUID_Parent == Text_SelectedNodeId))
                    {
                        SendTreeLevel(serviceAgentElastic.ClassList, Text_SelectedNodeId);
                        ViewTreeNodes_PathNodes = viewTreeFactory.CreatePathNodes(serviceAgentElastic.ClassList, Text_SelectedNodeId);
                    }

                }
                else if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.ViewModel_Text_Search)
                {
                    Text_Search = webSocketServiceAgent.ChangedProperty.Value.ToString();

                }
            }


        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;

        }

        private void SendTreeLevel(List<clsOntologyItem> levelItemList, string idParent = null)
        {
            if (levelItemList == null) return;

            var result = viewTreeFactory.CreateTreeNodes(levelItemList, 1, idParent);
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return;
            }

            var treeFileName = Guid.NewGuid().ToString() + ".json";
            var sessionFile = webSocketServiceAgent.RequestWriteStream(treeFileName);
            if (sessionFile.FileUri == null)
            {
                return;
            }

            if (sessionFile.StreamWriter == null)
            {

                return;
            }

            using (sessionFile.StreamWriter)
            {
                result = viewTreeFactory.WriteJsonDoc(sessionFile.StreamWriter);

            }
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {

                return;
            }


            Url_TreeData = sessionFile.FileUri.AbsoluteUri;
        }


        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;

        }
    

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }

    }
}
