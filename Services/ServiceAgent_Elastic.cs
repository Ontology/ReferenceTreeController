﻿using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using ReferenceTreeController.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ReferenceTreeController.Services
{
    public class ServiceAgent_Elastic : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private List<clsOntologyItem> classList;
        public List<clsOntologyItem> ClassList
        {
            get
            {
                return classList;
            }
        }

        private clsOntologyItem resultData;
        public clsOntologyItem ResultData
        {
            get { return resultData; }
            set
            {
                resultData = value;
                RaisePropertyChanged(NotifyChanges.Elastic_ResultData);
            }
        }

        private Thread thread_GetData;

        public List<clsOntologyItem> GetDataLevel(int subLevels, string idParent = null)
        {
            classList = new List<clsOntologyItem>();
            var result = localConfig.Globals.LState_Success.Clone();

            var dbReaderClasses = new OntologyAppDBConnector.OntologyModDBConnector(localConfig.Globals);
            var searchClasses = new List<clsOntologyItem>();


            do
            {
                if (!classList.Any())
                {
                    searchClasses.Add(new clsOntologyItem
                    {
                        GUID_Parent = idParent == null ? localConfig.Globals.Root.GUID : idParent
                    });
                }
                else
                {
                    searchClasses = dbReaderClasses.Classes1.Select(cls => new clsOntologyItem { GUID_Parent = cls.GUID }).ToList();
                }
                result = dbReaderClasses.GetDataClasses(searchClasses);
                classList.AddRange(dbReaderClasses.Classes1.Select(cls => cls.Clone()));
                subLevels--;
            } while (subLevels >= 0);


            return ClassList;

        }

        public clsOntologyItem GetDataClasses()
        {
            try
            {
                if (thread_GetData != null)
                {
                    thread_GetData.Abort();
                }
            }
            catch (Exception ex) { }

            thread_GetData = new Thread(GetDataAllAsync);
            thread_GetData.Start();

            return localConfig.Globals.LState_Success.Clone();
        }

        private void GetDataAllAsync()
        {
            var result = localConfig.Globals.LState_Success.Clone();

            var dbReaderClasses = new OntologyAppDBConnector.OntologyModDBConnector(localConfig.Globals);
            result = dbReaderClasses.GetDataClasses();
            classList = dbReaderClasses.Classes1;

            ResultData = result;
        }

        public ServiceAgent_Elastic(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;

            Initialize();
        }

        private void Initialize()
        {

        }
    }
}
