﻿using Newtonsoft.Json;
using ReferenceTreeController.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ReferenceTreeController.Models
{
    public class ViewTreeNode
    {
        [JQTree(DataAttribute ="id")]
        public string Id { get; set; }

        [JQTree(DataAttribute = "label")]
        public string Name { get; set; }

        [JQTree(DataAttribute = "items", SubNodes = true)]
        public List<ViewTreeNode> SubNodes { get; set; }
        public ViewTreeNode ParentNode { get; set; }

        private int childCount;
        [JQTree(DataAttribute = "childcount")]
        public int ChildCount
        {
            get
            {
                return childCount;
            }
            set
            {
                childCount = value;
            }
        }

        private string html;
        [JQTree(DataAttribute = "html")]
        public string Html
        {
            get
            {
                return html;
            }
            set
            {
                html = value;
            }
        }

        public string IdNodeImage { get; set; }

        //[DhtmlXTree(DataAttribute = "im2", OrderId = 5)]
        public string NameNodeImage { get; set; }

        //[DhtmlXTree(DataAttribute = "im1", OrderId = 4)]
        public string NameNodeImageSelected
        {
            get { return NameNodeImage; }
        }

        public string NameNodeImageNoChildren
        {
            get { return NameNodeImage != null ?  NameNodeImage + ".png" : null; }
        }

        public string IdPath { get; set; }

        public bool WriteJSON(JsonTextWriter jsonWriter)
        {
            try
            {
                jsonWriter.WriteStartObject();
                this.GetType().GetProperties().Cast<PropertyInfo>().ToList().ForEach(propItem =>
                {
                    var attribute = propItem.GetCustomAttribute<JQTreeAttribute>();
                    var value = propItem.GetValue(this);
                    if (value == null) return;
                    if (attribute != null)
                    {
                        jsonWriter.WritePropertyName(attribute.DataAttribute);
                        if (!attribute.SubNodes)
                        {
                            jsonWriter.WriteValue(value.ToString());
                        }
                        else
                        {
                            jsonWriter.WriteStartArray();
                            SubNodes.OrderBy(subNode => subNode.Name).ToList().ForEach(subNode =>
                            {
                                subNode.WriteJSON(jsonWriter);
                            });
                            jsonWriter.WriteEndArray();
                        }
                        
                        

                    }


                });


                jsonWriter.WriteEndObject();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
